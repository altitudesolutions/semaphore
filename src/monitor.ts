import * as Async from "@altitude/async";
import * as ErrorMessage from "@altitude/error-message";
import { Observable, Subject } from "rxjs";
import { filter, map, shareReplay, takeWhile } from "rxjs/operators";

import { Semaphore } from "./semaphore";
/**
 * An extension of `Semaphore` which constrains the registration of monitored observables to a preparation phase.
 * It exposes both an error event, and a completion event which reports aggregate success or failure.
 */
export class Monitor extends Semaphore {
    private readonly _completed = new Subject<boolean>();
    private _completed$: Observable<boolean> | undefined;
    private _failed = false;
    private _preparing = false;
    private _stack: Subject<void>[] | undefined;

    /**
     * Returns `true` if the instance is in the preparing state, otherwise return `false` (see `enterPrepare` and `exitPrepare`).
     */
    get isPreparing(): boolean {
        return this._preparing;
    }

    dispose(): void {
        Async.safeUnsubscribe(this._completed);
        super.dispose();
    }
    /**
     * Causes the instance to enter a preparation state, during which the completed and success events will not be raised
     * (irrespective of the active count). The preparation state ensures that the instance will not report completion should
     * all of its monitored observables complete before the full set of dependent observables have been registered for tracking.
     */
    enterPrepare(): void {
        this.disposedGuard();
        notPreparingGuard(this);
        this._preparing = true;
    }
    /**
     * Causes the instance to exit the preparation state. The instance will not report completion until after `exitPrepare`
     * has been invoked, irrespective of the active count.
     */
    exitPrepare(): void {
        this.disposedGuard();
        preparingGuard(this);
        this._preparing = false;
        this.checkState();
    }
    /**
     * Returns a contextual observable which will emit when all observables monitored by the instance have either
     * completed or errored. The returned observable emits `true` if all monitored observables emitted without error,
     * otherwise emits `false`. It will be detached and no longer emit following the invocation of the `reset()` method.
     */
    getCompletionEvent(): Observable<boolean> {
        this.disposedGuard();
        if (this._completed$ == undefined) {
            const context: Observable<boolean> = this._completed.pipe(
                // The context ensures that any observers of earlier sessions (prior to a reset) are detached.
                takeWhile(() => context === this._completed$),
                shareReplay({ refCount: true, bufferSize: 1 })
            );
            this._completed$ = context;
        }

        return this._completed$;
    }
    /**
     * Returns a contextual observable which will emit when all observables monitored by the instance have completed without
     * error. The returned observable will be detached and no longer emit following the invocation of the `reset()` method.
     */
    getSuccessEvent(): Observable<void> {
        this.disposedGuard();

        return this.getCompletionEvent().pipe(
            filter(success => success),
            map(() => undefined),
            shareReplay({ refCount: true, bufferSize: 1 })
        );
    }
    monitor(...observables: Observable<any>[]): void {
        preparingGuard(this);
        super.monitor(...observables);
    }
    /**
     * Pops an observable off the local stack and causes it to emit.
     */
    pop(): void {
        this.disposedGuard();
        if (this._stack == undefined || this._stack.length === 0) {
            throw new Error(
                ErrorMessage.invalidOperation(
                    "Invalid attempt to pop from empty stack."
                )
            );
        }
        const obs = this._stack.pop();
        if (obs != undefined) obs.next();
    }
    /**
     * Pushes an observable onto the local stack and monitors it. This feature can be useful for "manual" tracking.
     */
    push(): void {
        this.disposedGuard();
        preparingGuard(this);
        if (this._stack == undefined) {
            this._stack = [];
        }
        const s = new Subject<void>();
        this._stack.push(s);
        this.monitor(s);
    }
    reset(): void {
        super.reset();
        this._stack = undefined;
        this._preparing = false;
        this._failed = false;
        this._completed$ = undefined;
    }
    protected onComplete(): void {
        this.checkState();
    }
    protected onError(e: any): void {
        this._failed = true;
        this.checkState();
    }
    private checkState(): void {
        if (this._preparing) return;
        if (this.activeCount === 0) {
            this._completed.next(!this._failed);
        }
    }
}
function notPreparingGuard(m: Monitor): void {
    if (m.isPreparing) {
        throw new Error(
            ErrorMessage.invalidOperation("Must not be in the preparing state.")
        );
    }
}
function preparingGuard(m: Monitor): void {
    if (!m.isPreparing) {
        throw new Error(ErrorMessage.invalidOpOutsideState("preparing", m));
    }
}
