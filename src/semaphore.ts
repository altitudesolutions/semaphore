import * as Async from "@altitude/async";
import { Disposables, IDisposer } from "@altitude/disposables";
import * as ErrorMessage from "@altitude/error-message";
import {
    BehaviorSubject,
    isObservable,
    Observable,
    Subject,
    Subscription
} from "rxjs";
/**
 * Monitors any number of observables individually until they each emit next and are consequently detached.
 * The `Semaphore` is set and remains so for as long as one or more monitored observables have not emitted,
 * and is unset when no pending monitored observables remain attached.
 */
export class Semaphore {
    /**
     * Returns the number of active observables being tracked by the instance.
     */
    get activeCount(): number {
        return this._activeCount;
    }
    /**
     * A `Disposer` which subclasses may use to register disposable resources.
     * Note that the `Disposer` instance is disposed whenever `reset()` is invoked.
     */
    protected get disposer(): IDisposer {
        this.disposedGuard();

        return this._disposables;
    }

    /**
     * Returns an observable which emits any error raised by a tracked action.
     */
    get error$(): Observable<any> {
        this.disposedGuard();
        if (this._error == undefined) {
            this._error = new Subject();
            this._error$ = this._error.asObservable();
        }

        return this._error$;
    }
    /**
     * Returns `true` if the instance has been disposed, otherwise returns `false`.
     */
    get isDisposed(): boolean {
        return this._disposables == undefined;
    }
    /**
     * Returns `true` if one or more observables are being actively monitored, otherwise returns `false`.
     */
    get isSet(): boolean {
        return this._activeCount > 0;
    }
    /**
     * Emits `true` if one or more observables are being actively monitored, otherwise emits `false`.
     */
    get state$(): Observable<boolean> {
        this.disposedGuard();
        if (this._state == undefined) {
            this._state = new BehaviorSubject(this.isSet);
            this._state$ = this._state.asObservable();
        }

        return this._state$;
    }
    private _activeCount = 0;
    private readonly _disposables = new Disposables();
    private _error: Subject<any>;
    private _error$: Observable<any>;
    private _state: BehaviorSubject<boolean>;
    private _state$: Observable<boolean>;

    /**
     * Releases any resources held by the instance.
     */
    dispose(): void {
        if (this.isDisposed) return;
        this._disposables.dispose();
        (<any>this._disposables) = undefined;
        Async.safeUnsubscribe(this._state);
        Async.safeUnsubscribe(this._error);
    }
    /**
     * Monitors each of the specified observables until they next emit.
     * @param observables The observables to monitor.
     */
    monitor(...observables: Observable<any>[]): void {
        this.disposedGuard();
        observables.forEach(target => {
            if (target == undefined) {
                throw new Error(
                    ErrorMessage.argument(
                        "observables",
                        "Passed a null observable."
                    )
                );
            }
            if (!isObservable(target)) {
                throw new Error(
                    ErrorMessage.argument(
                        "observables",
                        "Passed an object which is not an Observable."
                    )
                );
            }
        });
        observables.forEach(target => {
            this._activeCount += 1;
            this.next();
            // Important! Define sub here to prevent temporal reference error occurring in subscription body!
            // Do NOT declare const sub = subscribeOnce(observable, ...
            let sub: Subscription;
            sub = this._disposables.subscribeOnce(
                target,
                // The observable which subscribeOnce subscribes to internally will always complete after the first emit,
                // so we handle `completed` rather than `next` here.
                {
                    complete: () => {
                        this.release(sub);
                        this.onComplete();
                    },
                    error: e => {
                        this.release(sub);
                        this.onError(e);
                        if (this._error != undefined) {
                            this._error.next(e);
                        }
                    }
                }
            );
        });
    }
    /**
     * Detaches from all monitored observables, unsubscribes from all internal subscriptions, and resets the instance's internal state.
     */
    reset(): void {
        this.disposedGuard();
        this._activeCount = 0;
        this._disposables.dispose();
        this.next();
    }

    /**
     * Throws an error if the instance has been disposed.
     */
    protected disposedGuard(): void {
        if (this.isDisposed) {
            throw new Error(ErrorMessage.invalidOperation("Object disposed."));
        }
    }
    /**
     * Invoked whenever a monitored observable emits. May overidden by subclasses to handle such events.
     */
    // tslint:disable-next-line: no-empty
    protected onComplete(): void {}
    /**
     * Invoked whenever a monitored observable errors. May overidden by subclasses to handle such events.
     */
    // tslint:disable-next-line: no-empty
    protected onError(_error?: any): void {}

    private next(): void {
        const isSet = this.isSet;
        if (this._state != undefined && this._state.value !== isSet) {
            this._state.next(isSet);
        }
    }

    private release(sub: Subscription): void {
        this._activeCount -= 1;
        this._disposables.disposeOf(sub);
        this.next();
    }
}
