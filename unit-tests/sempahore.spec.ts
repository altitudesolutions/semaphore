import * as ErrorMessage from "@altitude/error-message";
import { Observable, Subject } from "rxjs";

import { Semaphore } from "../src/semaphore";

/* tslint:disable:no-magic-numbers*/

describe("Semaphor", () => {
    it("Should be unset on success", () => {
        const s = new Semaphore();
        const trigger = new Subject<void>();
        expectCount(s, 0);
        s.monitor(trigger);
        expectFlag(s, true);
        expectCount(s, 1);
        trigger.next();
        expectFlag(s, false);
        expectCount(s, 0);
    });

    it("Should unset on error", () => {
        const s = new Semaphore();
        const trigger = new Subject<void>();
        expectCount(s, 0);
        s.monitor(trigger);
        expectFlag(s, true);
        expectCount(s, 1);
        trigger.error("Failed");
        expectFlag(s, false);
        expectCount(s, 0);
    });

    it("Should throw on null or undefined target", () => {
        const msg = ErrorMessage.argument(
            "observables",
            "Passed a null observable."
        );
        const s = new Semaphore();
        expect(() => s.monitor(getUndefined())).toThrowError(msg);
        expect(() => s.monitor(new Subject(), getUndefined())).toThrowError(
            msg
        );
        expect(() => s.monitor(getNull())).toThrowError(msg);
        expect(() => s.monitor(new Subject(), getNull())).toThrowError(msg);
        expect(s.activeCount).toBe(0);
    });

    it("Should throw on invalid target", () => {
        const msg = ErrorMessage.argument(
            "observables",
            "Passed an object which is not an Observable."
        );
        const s = new Semaphore();
        expect(() => s.monitor(<any>new Object())).toThrowError(msg);
        expect(() => s.monitor(<any>new Object(), new Subject())).toThrowError(
            msg
        );
        expect(() =>
            s.monitor(
                new Subject(),
                <any>new Object(),
                getUndefined<Observable<any>>()
            )
        ).toThrowError(msg);
        expect(s.activeCount).toBe(0);
    });
    it("Should emit errors", () => {
        let errors = 0;
        const triggers = [
            new Subject<void>(),
            new Subject<void>(),
            new Subject<void>()
        ];
        const s = new Semaphore();
        s.error$.subscribe(e => {
            expect(e)
                .withContext("Unexpected error value")
                .toBe(errors.toString());
            errors += 1;
        });
        s.monitor(...triggers);
        expect(s.isSet).toBe(true);
        triggers[1].error("0");
        triggers[2].error("1");
        triggers[0].error("2");
        expect(errors).toBe(triggers.length);
        expect(s.isSet).toBe(false);
    });
});

function expectCount(s: Semaphore, count: number): void {
    expect(s.activeCount)
        .withContext(`activeCount should be ${count}`)
        .toBe(count);
}

function expectFlag(s: Semaphore, flag: boolean): void {
    expect(s.isSet)
        .withContext(flag ? "Flag should not be set" : "Flag should not be set")
        .toBe(flag);
}

function getNull<T>(): T {
    // tslint:disable-next-line: no-null-keyword
    return <T>(<unknown>null);
}

function getUndefined<T>(): T {
    return <T>(<unknown>undefined);
}
