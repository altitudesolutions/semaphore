import * as ErrorMessage from "@altitude/error-message";
import { Subject } from "rxjs";

import { Monitor } from "../src";

/* tslint:disable:no-magic-numbers*/

describe("Monitor", () => {
    it("Unused out-of-context success event should not emit after reset()", () => {
        let count = 0;
        const trigger = new Subject<void>();
        const m = new Monitor();
        const obs = m.getSuccessEvent();
        obs.subscribe(() => {
            count += 1;
        });
        m.enterPrepare();
        m.monitor(trigger);
        m.exitPrepare();
        m.reset();
        m.enterPrepare();
        m.monitor(trigger);
        m.exitPrepare();
        trigger.next();
        expect(count).toBe(0);
    });

    it("Should emit true on success", () => {
        let outcome: string | undefined;
        const trigger = new Subject<void>();
        const m = new Monitor();
        m.getCompletionEvent().subscribe(succeeded => {
            outcome = succeeded ? "success" : "failure";
        });
        m.enterPrepare();
        m.monitor(trigger);
        m.exitPrepare();
        trigger.next();
        expect(outcome).toBe("success");
    });

    it("Should emit false on failure", () => {
        let outcome: string | undefined;
        const trigger = new Subject<void>();
        const m = new Monitor();
        m.getCompletionEvent().subscribe(succeeded => {
            outcome = succeeded ? "success" : "failure";
        });
        m.enterPrepare();
        m.monitor(trigger);
        m.exitPrepare();
        trigger.error("error");
        expect(outcome).toBe("failure");
    });
    it("Should emit errors", () => {
        let errors = 0;
        const triggers = [
            new Subject<void>(),
            new Subject<void>(),
            new Subject<void>()
        ];
        const m = new Monitor();
        m.error$.subscribe(e => {
            expect(e)
                .withContext("Unexpected error value")
                .toBe(errors.toString());
            errors += 1;
        });
        m.enterPrepare();
        m.monitor(...triggers);
        m.exitPrepare();
        expect(m.isSet).toBe(true);
        triggers[1].error("0");
        triggers[2].error("1");
        triggers[0].error("2");
        expect(errors).toBe(triggers.length);
        expect(m.isSet).toBe(false);
    });
    it("Should throw if not preparing", () => {
        const msg = ErrorMessage.invalidOperation(
            "The operation is invalid outside of the Monitor preparing state."
        );
        const m = new Monitor();
        expect(() => m.exitPrepare()).toThrowError(msg);
        expect(() => m.push()).toThrowError(msg);
        expect(() => m.monitor()).toThrowError(msg);
    });
    it("Should throw if preparing", () => {
        const msg = ErrorMessage.invalidOperation(
            "Must not be in the preparing state."
        );
        const m = new Monitor();
        m.enterPrepare();
        expect(() => m.enterPrepare()).toThrowError(msg);
    });
    it("Should throw if popping from an empty stack", () => {
        const msg = ErrorMessage.invalidOperation(
            "Invalid attempt to pop from empty stack."
        );
        const m = new Monitor();
        expect(() => m.pop()).toThrowError(msg);
    });
    it("Should reflect preparing state", () => {
        const m = new Monitor();
        expect(m.isPreparing).toBe(false);
        m.enterPrepare();
        expect(m.isPreparing).toBe(true);
        m.exitPrepare();
        expect(m.isPreparing).toBe(false);
    });
});
